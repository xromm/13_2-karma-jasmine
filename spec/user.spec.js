import {sum} from './user';

describe('Users factory', function() {
  it('has a dummy spec to test 2 + 2', function() {
    expect(4).toEqual(4);
  });
  it('has a dummy spec to test 2 + 2 2', function() {
    expect(sum(2,2)).toEqual(4);
  });
});
