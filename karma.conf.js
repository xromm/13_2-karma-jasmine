module.exports = function(config) {
    config.set({
      basePath: '',
      frameworks: ['jasmine'],
      files: [
        'spec/**/*.spec.js'
      ],
      preprocessors: {
        'spec/**/*.spec.js': ['webpack']
      },
      webpack: {
        module: {
          loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
          }]
        },
        resolve: {
          extensions: ['', '.js']
        }
      },

      webpackMiddleware: {
        // webpack-dev-middleware configuration
        // i. e.
        stats: 'errors-only'
      },
      //reporters: ['mocha'],
      port: 9876,
      colors: true,
      logLevel: config.LOG_INFO,
      autoWatch: true,
      browsers: ['Chrome'],
      singleRun: true,
      concurrency: Infinity
    })
  }
  //
  // module.exports = function(config) {
  //   config.set({
  //
  //     // base path that will be used to resolve all patterns (eg. files, exclude)
  //     basePath: '',
  //
  //
  //     // frameworks to use
  //     // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
  //     frameworks: ['jasmine'],
  //
  //
  //     // list of files / patterns to load in the browser
  //     files: [
  //       './spec/user.spec.js'
  //     ],
  //
  //
  //     // list of files to exclude
  //     exclude: [
  //     ],
  //
  //
  //     // preprocess matching files before serving them to the browser
  //     // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
  //     preprocessors: {
  //       './spec/user.spec.js': ['babel']
  //     },
  //
  //
  //     // test results reporter to use
  //     // possible values: 'dots', 'progress'
  //     // available reporters: https://npmjs.org/browse/keyword/karma-reporter
  //     reporters: ['progress'],
  //
  //
  //     // web server port
  //     port: 9876,
  //
  //
  //     // enable / disable colors in the output (reporters and logs)
  //     colors: true,
  //
  //
  //     // level of logging
  //     // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
  //     logLevel: config.LOG_INFO,
  //
  //
  //     // enable / disable watching file and executing tests whenever any file changes
  //     autoWatch: true,
  //
  //
  //     // start these browsers
  //     // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
  //     browsers: ['Chrome'],
  //
  //
  //     // Continuous Integration mode
  //     // if true, Karma captures browsers, runs the tests and exits
  //     singleRun: false,
  //
  //     // Concurrency level
  //     // how many browser should be started simultaneous
  //     concurrency: Infinity,
  //
  //     // webpack config object
  //     webpack: {
  //       devtool: 'inline-source-map',
  //       module: {
  //         loaders: [
  //           {
  //             exclude: /node_modules/,
  //             loader: 'babel-loader',
  //             test: /\.jsx?$/
  //           }
  //         ],
  //       }
  //     },
  //     webpackMiddleware: {
  //       noInfo: true,
  //     }
  //   })
  // }
